/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author sumsung
 */
public class Customer {

    private int id;
    private String phone;
    private String name;
    private int come;

    public Customer(int id, String name, String phone, int come) {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.come = come;
    }
    
    public Customer(String name, String tel,int come) {
        this(-1, name, tel,come);
    }
    
    public Customer(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCome() {
        return come;
    }

    public void setCome(int come) {
        this.come = come;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", phone=" + phone + ", name=" + name + ", come=" + come + '}';
    }

}
