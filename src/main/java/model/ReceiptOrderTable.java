/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sumsung
 */
public class ReceiptOrderTable extends AbstractTableModel{
    private ArrayList<Receipt> data;
        String[] columName = {"Order Code","Queue", "Date", "Staff ID", "Customer ID", "Total Price(Bath)","Cash(Bath)","Change(Bath)","Discount(Bath)"};
    
public ReceiptOrderTable(ArrayList<Receipt> data) {
            this.data = data;
        }
        
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 9;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Receipt orderData = data.get(rowIndex);
        if (columnIndex == 0) {
                return orderData.getId();
            }
            if (columnIndex == 1) {
                return orderData.getQueue();
            }

            if (columnIndex == 2) {
                return orderData.getDate();
            }
            
            if (columnIndex == 3) {
                return orderData.getUser().getId();
            }
            if (columnIndex == 4) {
                return orderData.getCustomer().getId();
            }
            if (columnIndex == 5) {
                return orderData.getTotal();
            }
            if (columnIndex == 6) {
                return orderData.getReciveCash();
            }
            if (columnIndex == 7) {
                return orderData.getChangeCash();
            }
            if (columnIndex == 8) {
                return orderData.getDiscount();
            }
            return "";
    }
    
    @Override
        public String getColumnName(int column) {
            return columName[column];
        }
}
