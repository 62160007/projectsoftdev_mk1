/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sumsung
 */
public class AttendanceTable extends AbstractTableModel {

    private ArrayList<Attendance> data;
    String[] columName = {"UserID", "UserName","TotalTime","SalaryRate", "TotalSalary"};

    public AttendanceTable(ArrayList<Attendance> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Attendance atdData = data.get(rowIndex);
        if (columnIndex == 0) {
            return atdData.getUser().getId();
        }
        if (columnIndex == 1) {
            return atdData.getUser().getShowName();
        }

        if (columnIndex == 2) {
            return atdData.getTotalTime();
        }
        if(columnIndex == 3){
            return atdData.getUser().getSalaryRate();
        }
        if(columnIndex == 4){
            return atdData.getTotalTime()*atdData.getUser().getSalaryRate();
        }
        return "";
    }
    
    @Override
    public String getColumnName(int column) {
            return columName[column];
    }
}
