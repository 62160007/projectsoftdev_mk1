/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sumsung
 */
public class UserTable extends AbstractTableModel{
    private ArrayList<User> data;
        String[] columName = {"Staff ID", "Employee UserName", "Employee Password", "Employee Name", "Salary Rate","Employee Type"};
    
public UserTable(ArrayList<User> data) {
            this.data = data;
        }
        
    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        User userData = data.get(rowIndex);
        if (columnIndex == 0) {
                return userData.getId();
            }
            if (columnIndex == 1) {
                return userData.getName();
            }

            if (columnIndex == 2) {
                return userData.getPassword();
            }
            
            if (columnIndex == 3) {
                return userData.getShowName();
            }
            if (columnIndex == 4) {
                return userData.getSalaryRate();
            }
            if (columnIndex == 5) {
                return userData.getType();
            }
            return "";
    }
    
    @Override
        public String getColumnName(int column) {
            return columName[column];
        }
    
}
