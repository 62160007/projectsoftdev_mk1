/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author sumsung
 */
// edit file name
public class SalaryPayment {
    private int id;
    private User user;
    private double salary;
    private String bookbank;
    private Date date;
    private String check;

    public SalaryPayment(int id, User user, double salary, String bookbank, Date date, String check) {
        this.id = id;
        this.user = user;
        this.salary = salary;
        this.bookbank = bookbank;
        this.date = date;
        this.check = check;
    }

    public SalaryPayment(User user, double salary, String bookbank, String check) {
        this(-1,user,salary,bookbank,null,check);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getBookbank() {
        return bookbank;
    }

    public void setBookbank(String Bookbank) {
        this.bookbank = Bookbank;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String Check) {
        this.check = Check;
    }

    @Override
    public String toString() {
        return "SalaryPayMent{" + "id=" + id + ", user=" + user + ", salary=" + salary + ", Bookbank=" + bookbank + ", date=" + date + ", Check=" + check + '}';
    }
}
