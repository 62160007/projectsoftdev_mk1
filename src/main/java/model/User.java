/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author patpunyanuch
 */
public class User {
    
    private int id;
    private String name;
    private String password;
    private String showName;
    private double salaryRate;
    private String type;

    public User(int id, String name, String password, String showName, double salaryRate, String type) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.showName = showName;
        this.salaryRate = salaryRate;
        this.type = type;
    }
    
    public User(int id, String name, String showName, double salaryRate, String type) {
        this(id, name, "", showName, salaryRate, type);
    }

    public User(String name, String password, String showName, double salaryRate, String type) {
        this(-1, name, password, showName, salaryRate, type);
    }

    public User(String name, String showName, double salaryRate, String type) {
        this(-1, name, "", showName, salaryRate, type);
    }
    public User(int id){
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public double getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryrate(double salaryRate) {
        this.salaryRate = salaryRate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", password=" + password + ", showName=" + showName + ", salaryRate=" + salaryRate + ", type=" + type + '}';
    }
    
}
