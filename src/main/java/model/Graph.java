/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import java.awt.Color;
/**
 *
 * @author tatar
 */
public class Graph {
	private String title;
	private int amount;
	private Color color;
	public Graph(String title,int amount,Color color) {
		this.title=title;
		this.amount=amount;
		this.color=color;
	}
	
        public Graph(){
            
        }
        
	public String getTitle() {
		return title;
	}
	public int getAmount() {
		return amount;
	}
	public Color getColor() {
		return color;
	}
        public Color getColor(int index) {
            if(index==0){
                return Color.red;
            }
            else if(index==1){
                return Color.blue;
            }
            else if(index==2){
                return Color.green;
            }
            else if(index==3){
                return Color.yellow;
            }
            else if(index==4){
                return Color.magenta;
            }
            else if(index==5){
                return Color.pink;
            }
            else if(index==6){
                return Color.orange;
            }
            else if(index==7){
                return Color.cyan;
            }
            else if(index==8){
                return Color.gray;
            }
            else if(index==9){
                return Color.black;
            }
            else{
                return Color.red;
            }
        }
}