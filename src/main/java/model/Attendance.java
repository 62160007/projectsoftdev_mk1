/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author sumsung
 */
public class Attendance {

    private int id;
    private User user;
    private Date timeAttendance;
    private Date timeOff;
    private double totalTime;

    public Attendance(int id, User user, Date timeAttendance, Date timeOff,double totalTime) {
        this.id = id;
        this.user = user;
        this.timeAttendance = timeAttendance;
        this.timeOff = timeOff;
        this.totalTime += totalTime;
    }

    public Attendance(int id,User user,double totalTime) {
        this(id,user,null,null,totalTime);
    }
    
    public Attendance(User user,double totalTime) {
        this(-1,user,null,null,totalTime);
    }
    
    public Attendance(int id,double totalTime) {
        this.id = id;
        this.totalTime = totalTime;
    }
/*
    public Attendance(int id, int userId, java.util.Date timeIn, java.util.Date timeOut, double totalTime) {
        this.id=id;
        //this.user=user;
        this.timeAttendance=(Date) timeIn;
        this.timeOff=(Date) timeOut;
        this.totalTime=totalTime;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTimeAttendance() {
        return timeAttendance;
    }

    public void setTimeAttendance(Date timeAttendance) {
        this.timeAttendance = timeAttendance;
    }

    public Date getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(Date timeOff) {
        this.timeOff = timeOff;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    @Override
    public String toString() {
        return "Attendance{" + "id=" + id + ", user=" + user + ", timeAttendance=" + timeAttendance + ", timeOff=" + timeOff + ", totalTime=" + totalTime + '}';
    }
}
