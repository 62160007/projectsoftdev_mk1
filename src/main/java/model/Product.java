/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Dao.ProductDao;
import java.util.ArrayList;

/**
 *
 * @author sumsung
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private int min;
    private String image;
    private String Type;

    public Product(int id, String name, double price, String image, String Type) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.Type = Type;
    }

    public Product(int id) {
        this.id = id;
    }

    public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", min=" + min + ", image=" + image + ", Type=" + Type + '}';
    }

    public String showSellProduct() {
        return name + " " + price + " x ";
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public static ArrayList<Product> coffeeProductList() {

        ArrayList<Product> productList = new ArrayList<>();
        ProductDao dao = new ProductDao();
        productList = dao.getAll();
        ArrayList<Product> list = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getType().equals("Coffee")) {
                list.add(productList.get(i));
            }
        }
//        list.add(new Product(1 , "Hot Americano", 35, "Hot Americano.jpg"));
//        list.add(new Product(2 , "Hot Latte", 50, "Hot Latte.jpg"));
//        list.add(new Product(3 , "Hot Mocha", 50, "Hot Mocha.jpg"));
//        list.add(new Product(4 , "Hot Cappuccino", 45, "Hot Cappuccino.jpg"));
//        list.add(new Product(5 , "Hot Cocoa", 40, "Hot Cocoa.jpg"));
//        list.add(new Product(6 , "Hot Fresh Milk", 35, "Hot Fresh Milk.jpg"));
//        list.add(new Product(7 , "Hot Green Tea", 40, "Hot Green Tea.jpg"));
//        list.add(new Product(8 , "Hot Thai Tea", 40, "Hot Thai Tea.jpg"));
//        list.add(new Product(9 , "Iced Espresso", 55, "Iced Espresso.jpg"));
//        list.add(new Product(10 , "Iced Americano", 55, "Iced Americano.jpg"));
//        list.add(new Product(11 , "Iced Latte", 65, "Iced Latte.jpg"));
//        list.add(new Product(12 , "Iced Mocha", 65, "Iced Mocha.jpg"));
//        list.add(new Product(13 , "Iced Cappuccino", 60, "Iced Cappuccino.jpg"));
//        list.add(new Product(14 , "Iced Cocoa", 50, "Iced Cocoa.jpg"));
//        list.add(new Product(15 , "Iced Green Tea", 50, "Iced Green Tea.jpg"));
//        list.add(new Product(16 , "Iced Thai Tea", 50, "Iced Thai Tea.jpg"));
//        list.add(new Product(17 , "Iced Fresh Milk", 45, "Iced Fresh Milk.jpg"));
//        list.add(new Product(18 , "Iced Pinky Milk", 45, "Iced Pinky Milk.jpg"));
        return list;
    }

    public static ArrayList<Product> juicesProductList() {
        ArrayList<Product> productList = new ArrayList<>();
        ProductDao dao = new ProductDao();
        productList = dao.getAll();
        ArrayList<Product> list = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getType().equals("Juices & Smoothies")) {
                list.add(productList.get(i));
            }
        }
//        list.add(new Product(1 , "Blue Pea Lemon Juice", 45, "Blue Pea Lemon Juice.jpg"));
//        list.add(new Product(2 , "Strawberry Lychee Smoothie", 65, "Strawberry Lychee Smoothie.jpg"));
//        list.add(new Product(3 , "Kiwi Guava Smoothie", 65, "Kiwi Guava Smoothie.jpg"));
        return list;
    }

    public static ArrayList<Product> bakeryProductList() {
        ArrayList<Product> productList = new ArrayList<>();
        ProductDao dao = new ProductDao();
        productList = dao.getAll();
        ArrayList<Product> list = new ArrayList<>();
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getType().equals("Bakery")) {
                list.add(productList.get(i));
            }
        }
//        list.add(new Product(1 , "Strawberry Croissant Twist", 60, "Strawberry Croissant Twist.jpg"));
//        list.add(new Product(2 , "Sandwich", 55, "Sandwich.jpg"));
//        list.add(new Product(3 , "Red Velvet Cake", 65, "Red Velvet Cake.jpg"));
//        list.add(new Product(4 , "Chocolate Devil", 60, "Chocolate Devil.jpg"));
        return list;
    }
}
