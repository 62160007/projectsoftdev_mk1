/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author tatar
 */
public class CumulativeSales {
    private String productId;
    private String productName;
    private int sum;
    
    public CumulativeSales(Product product,int sum){
        this.productId=""+product.getId();
        this.productName=""+product.getName();
        this.sum=sum;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
   

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "CumulativeSales{" + "productId=" + productId + ", productName=" + productName + ", sum=" + sum + '}';
    }

    
    
    
}
