/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import Dao.ReceiptDetailDao;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author patpunyanuch
 */
public class Receipt {
    
    private int id;
    private int queue;
    private Date date;
    private User user;
    private Customer customer;
    private double reciveCash;
    private double changeCash;
    private double discount;
    private double total;
    private ReceiptDetailDao dao = new ReceiptDetailDao();

    public Receipt(int id,int queue, Date date, User user, Customer customer, double reciveCash, double changeCash, double discount) {
        this.id = id;
        this.queue = queue;
        this.date = date;
        this.user = user;
        this.customer = customer;
        this.reciveCash = reciveCash;
        this.changeCash = changeCash;
        this.discount = discount;
        this.total = dao.getTotal(id);
    }
    public Receipt(int queue, Date date, User user, Customer customer, double reciveCash, double changeCash, double discount) {
        this.id = -1;
        this.queue = queue;
        this.date = date;
        this.user = user;
        this.customer = customer;
        this.reciveCash = reciveCash;
        this.changeCash = changeCash;
        this.discount = discount;
        this.total = -1;
    }
    public Receipt(int queue,User user, Customer customer, double reciveCash, double changeCash, double discount) {
        this(-1,queue,null,user,customer,reciveCash,changeCash,discount);
    }
    public Receipt(int id,int queue,User user, Customer customer, double reciveCash, double changeCash, double discount) {
        this(id,queue,null,user,customer,reciveCash,changeCash,discount);
    }
    public Receipt(int id) {
        this.id = id;
    }

    public Receipt(ArrayList<Receipt> receiptList) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getReciveCash() {
        return reciveCash;
    }

    public void setReciveCash(double receiptCash) {
        this.reciveCash = receiptCash;
    }

    public double getChangeCash() {
        return changeCash;
    }

    public void setChangeCash(double changeCash) {
        this.changeCash = changeCash;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ReceiptDetailDao getDao() {
        return dao;
    }

    public void setDao(ReceiptDetailDao dao) {
        this.dao = dao;
    }
    
    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", queue=" + queue + ", date=" + date + ", user=" + user + ", customer=" + customer + ", receiptCash=" + reciveCash + ", changeCash=" + changeCash + ", discount=" + discount + '}';
    }

}
