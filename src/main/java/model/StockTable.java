/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author sumsung
 */
public class StockTable extends AbstractTableModel{
    private ArrayList<Ingredient> data;
        String[] columName = {"ID", "Name", "Price", "amount", "unit","min"};

        public StockTable(ArrayList<Ingredient> data) {
            this.data = data;
        }

        @Override
        public int getRowCount() {
            return this.data.size();
        }

        @Override
        public int getColumnCount() {
            return 6;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Ingredient ingredientData = this.data.get(rowIndex);
            if (columnIndex == 0) {
                return ingredientData.getId();
            }
            if (columnIndex == 1) {
                return ingredientData.getName();
            }

            if (columnIndex == 2) {
                return ingredientData.getPrice();
            }
            
            if (columnIndex == 3) {
                return ingredientData.getAmount();
            }
            if (columnIndex == 4) {
                return ingredientData.getUnit();
            }
            if (columnIndex == 5) {
                return ingredientData.getMin();
            }
            return "";
        }

        @Override
        public String getColumnName(int column) {
            return columName[column];
        }
}
