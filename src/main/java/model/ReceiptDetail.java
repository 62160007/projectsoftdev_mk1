/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author patpunyanuch
 */
public class ReceiptDetail {

    private int id;
    private Receipt receipt;
    private Product product;
    private int amount;
    private double priceAmount;
    private double priceTotal;

    public ReceiptDetail(int id, Receipt receipt, Product product, int amount, double priceAmount, double priceTotal) {
        this.id = id;
        this.receipt = receipt;
        this.product = product;
        this.amount = amount;
        this.priceAmount = priceAmount;
        this.priceTotal = priceTotal;
    }
    public ReceiptDetail( Receipt receipt, Product product, int amount, double priceAmount, double priceTotal) {
        this.id = -1;
        this.receipt = receipt;
        this.product = product;
        this.amount = amount;
        this.priceAmount = priceAmount;
        this.priceTotal = priceTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPriceAmount() {
        return priceAmount;
    }

    public void setPriceAmount(double priceAmount) {
        this.priceAmount = priceAmount;
    }

    public double getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", receipt=" + receipt + ", product=" + product + ", amount=" + amount + ", priceAmount=" + priceAmount + ", priceTotal=" + priceTotal +'}';
    }

}
