/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.CumulativeSales;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.SalaryPayment;
import model.User;

/**
 *
 * @author tatar
 */
public class ReceiptDetailDao {
    public int add(ReceiptDetail object){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt_Detail (\n" +
"                               Receipt_ID,\n" +
"                               Product_ID,\n" +
"                               ReceiptDetail_Amount,\n" +
"                               ReceiptDetail_Price_Amount,\n" +
"                               ReceiptDetail_Price_Total\n" +
"                           )\n" +
"                           VALUES (\n" +
"                               ?,\n" +
"                               ?,\n" +
"                               ?,\n" +
"                               ?,\n" +
"                               ?\n" +
"                           );";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getReceipt().getId());
            stmt.setInt(2, object.getProduct().getId());
            stmt.setInt(3, object.getAmount());
            stmt.setDouble(4, object.getPriceAmount());
            stmt.setDouble(5, object.getPriceTotal());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create salarypayment.");
        }
        db.close();
        return id;
    }
    
    public ArrayList<ReceiptDetail> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT ReceiptDetail_ID,\n" +
                "       Receipt_ID,\n" +
                "       Product_ID,\n" +
                "       ReceiptDetail_Amount,\n" +
                "       ReceiptDetail_Price_Amount,\n" +
                "       ReceiptDetail_Price_Total\n" +
                "  FROM Receipt_Detail;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int receiptDetailId = result.getInt("ReceiptDetail_ID");
                //FK User
                int receiptId = result.getInt("Receipt_ID");
                int productId = result.getInt("Product_ID");
                int amount = result.getInt("ReceiptDetail_Amount");
                double price = result.getDouble("ReceiptDetail_Price_Amount");
                //FK User
                double total = result.getDouble("ReceiptDetail_Price_Total");

                ReceiptDetail receiptDetail = new ReceiptDetail(receiptDetailId,new Receipt(receiptId),new Product(productId),amount,price,total);
                list.add(receiptDetail);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ReceiptDetail!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }
    
    public ArrayList<CumulativeSales> getBestSell() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Product_ID,\n" +
                "        Product_Name,\n" +
                "       SUM(ReceiptDetail_Amount) as sumAll\n" +
                "  FROM Receipt_Detail NATURAL JOIN Product\n" +
                "  GROUP BY Product_ID \n" +
                "  ORDER BY sumAll DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int productId = result.getInt("Product_ID");
                String productName = result.getString("Product_Name");
                int amount = result.getInt("sumAll");

                Product product = new Product(productId,productName);
                CumulativeSales cumulativeSales = new CumulativeSales(product,amount);
                list.add(cumulativeSales);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ReceiptDetail!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }
    
    public ArrayList<CumulativeSales> getWorstSell() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Product_ID,\n" +
                "        Product_Name,\n" +
                "       SUM(ReceiptDetail_Amount) as sumAll\n" +
                "  FROM Receipt_Detail NATURAL JOIN Product\n" +
                "  GROUP BY Product_ID \n" +
                "  ORDER BY sumAll ASC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int productId = result.getInt("Product_ID");
                String productName = result.getString("Product_Name");
                int amount = result.getInt("sumAll");

                Product product = new Product(productId,productName);
                CumulativeSales cumulativeSales = new CumulativeSales(product,amount);
                list.add(cumulativeSales);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ReceiptDetail!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }
    public double getTotal(int id){
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT sum(ReceiptDetail_Price_Total) as sumTotal\n" +
"  FROM Receipt_Detail\n" +
"  WHERE Receipt_ID="+id+";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                double total = result.getDouble("sumtotal");

                return total;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ReceiptDetail!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return 0;
    }
    public ReceiptDetail get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT ReceiptDetail_ID,\n" +
                "       Receipt_ID,\n" +
                "       Product_ID,\n" +
                "       ReceiptDetail_Amount,\n" +
                "       ReceiptDetail_Price_Amount,\n" +
                "       ReceiptDetail_Price_Total\n" +
                "  FROM Receipt_Detail\n" +
                "WHERE ReceiptDetail_ID="+id+";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int receiptDetailId = result.getInt("ReceiptDetail_ID");
                //FK User
                int receiptId = result.getInt("Receipt_ID");
                int productId = result.getInt("Product_ID");
                int amount = result.getInt("ReceiptDetail_Amount");
                double price = result.getDouble("ReceiptDetail_Price_Amount");
                //FK User
                double total = result.getDouble("ReceiptDetail_Price_Total");

                ReceiptDetail receiptDetail = new ReceiptDetail(receiptDetailId,new Receipt(receiptId),new Product(productId),amount,price,total);
                return receiptDetail;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ReceiptDetail!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return null;
    }
    
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt_Detail\n" +
                "      WHERE ReceiptDetail_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect ReceiptDetail id " + id + "!!!");
        }
        db.close();
        return row;
    }
    public int deleteAllRecieptDetail(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt_Detail\n" +
                    "      WHERE Receipt_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect ReceiptDetail id " + id + "!!!");
        }
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        ReceiptDetailDao dao = new ReceiptDetailDao();
        Receipt receipt = new Receipt(1);
        Product product = new Product(1);
        //System.out.println(dao.add(new ReceiptDetail(receipt,product,3,30,90)));
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        //System.out.println(dao.delete(3));
        /*System.out.println(dao.getAll());
        System.out.println(dao.get(1));
           */
        System.out.println(dao.getBestSell());
        System.out.println(dao.getWorstSell());
    }
}
