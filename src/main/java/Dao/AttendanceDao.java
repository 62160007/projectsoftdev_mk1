/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Attendance;
import model.User;

/**
 *
 * @author sumsung
 */
public class AttendanceDao {

    public int add(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Attendance (\n" +
"                           User_ID,\n" +
"                           Attendance_Total_Time\n" +
"                       )\n" +
"                       VALUES (\n" +
"                           ?,\n" +
"                           0\n" +
"                       );";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, object.getUser().getId());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create attendance." + ex.getMessage());
        }
        db.close();     
        //calTotal(object.getUser().getId());
        //setLocalTime(object.getUser().getId(),0);
        return id;
    }
/*
    public ArrayList<Attendance> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Attendance_ID,\n"
                    + "       User_ID,\n"
                    + "       Attendance_Time_Attendance,\n"
                    + "       Attendance_Time_Off,\n"
                    + "       Attendance_Total_Time\n"
                    + "  FROM Attendance;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Attendance_ID");
                int userid = result.getInt("User_ID");
                Date timeIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Attendance"));
                Date timeOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Off"));
                double totalTime = result.getInt("Attendance_Total_Time");
                UserDao userDao = new UserDao();
                User user = userDao.get(id);
                Attendance attendance = new Attendance(id, user, timeIn, timeOut, totalTime);
                list.add(attendance);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all attendance!!!");
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }
*/  
    
    public ArrayList<Attendance> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Attendance_ID,\n" +
                    "       User_ID,\n" +
                    "       Attendance_Time_Attendance,\n" +
                    "       Attendance_Time_Off,\n" +
                    "       Attendance_Total_Time\n" +
                    "  FROM Attendance;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Attendance_ID");
                int userID = result.getInt("User_ID");
                Date timeIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Attendance"));
                Date timeOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Off"));
                double totalTime = result.getDouble("Attendance_Total_Time");
                UserDao userDao = new UserDao();
                User user = userDao.get(userID);
                Attendance attendance = new Attendance(id,user,timeIn,timeOut,totalTime);
                list.add(attendance);
                /*
                System.out.println(id);
                System.out.println(userID);
                System.out.println(timeIn);
                System.out.println(timeOut);
                System.out.println(totalTime);
                System.out.println(user);*/
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all attendance!!!");
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }
    
    public ArrayList<Attendance> getAllTimeTotal(int year,int month) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT User_ID,\n" +
                "       sum(Attendance_Total_Time) AS sumTotal\n" +
                "  FROM Attendance\n" +
                "  WHERE Attendance_Time_Attendance BETWEEN '"+year+"-"+month+"-01'AND'"+year+"-"+month+"-31'\n" +
                "  GROUP BY User_ID \n" +
                "  ORDER BY User_ID ASC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int userID = result.getInt("User_ID");
                double totalTime = result.getDouble("sumTotal");
                UserDao userDao = new UserDao();
                User user = userDao.get(userID);
                Attendance attendance = new Attendance(user,totalTime);
                list.add(attendance);
              
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all Total attendance!!!");
        } 
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }
    
    public Attendance getTimeTotal(int id,int month) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT User_ID,\n" +
                "       sum(Attendance_Total_Time) AS sumTotal\n" +
                "  FROM Attendance\n" +
                "  WHERE User_ID ="+id+" AND Attendance_Time_Attendance BETWEEN '2021-"+month+"-01'AND'2021-"+month+"-30'\n" +
                "  GROUP BY User_ID \n" +
                "  ORDER BY User_ID ASC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int userID = result.getInt("User_ID");
                double totalTime = result.getDouble("sumTotal");
                UserDao userDao = new UserDao();
                User user = userDao.get(userID);
                Attendance attendance = new Attendance(user,totalTime);
                
                return attendance;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select attendance id " + id + "!!!");
        } 
        db.close();
        return null;
    }
    
    public Attendance get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Attendance_ID,\n"
                    + "       User_ID,\n"
                    + "       Attendance_Time_Attendance,\n"
                    + "       Attendance_Time_Off,\n"
                    + "       Attendance_Total_Time\n"
                    + "  FROM Attendance\n"
                    + "  WHERE Attendance_ID = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int rid = result.getInt("Attendance_ID");
                int userid = result.getInt("User_ID");
                Date timeIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Attendance"));
                Date timeOut = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Attendance_Time_Off"));
                int totalTime = result.getInt("Attendance_Total_Time");
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select attendance id " + id + "!!!");
        } catch (ParseException ex) {
            Logger.getLogger(AttendanceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Attendance\n"
                    + "      WHERE Attendance_ID = ?; ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect attendance id " + id + "!!!");
        }
        db.close();
        return row;
    }

    public int update(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Attendance\n" +
                "   SET \n" +
                "       Attendance_Time_Off = CURRENT_TIMESTAMP\n" +
                " WHERE \n" +
                "       User_ID = ? AND \n" +
                "       Attendance_Time_Attendance BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            //stmt.setString(1, object.getTimeOff());
            stmt.setInt(1, object.getUser().getId());
            row = stmt.executeUpdate();
            
        } catch (SQLException ex) {
            System.out.println("Error : Unable to update attendance id!!!");
        }
        db.close();
        calTotal(object);
        setLocalTime(object.getUser().getId(),0);
        setLocalTime(object.getUser().getId(),1);
        
        return row;
    }
    
    public int setLocalTime(int index,int sel) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql="";
            if(sel==0){
                sql = "UPDATE Attendance\n" +
                        "   SET \n" +
                        "       Attendance_Time_Attendance = (\n" +
                        "        SELECT datetime(Attendance_Time_Attendance, 'localtime')\n" +
                        "        FROM Attendance\n" +
                        "        WHERE User_ID=? AND Attendance_Time_Attendance BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP\n" +
                        "       )\n" +       
                        " WHERE User_ID=? AND Attendance_Time_Attendance BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP; ";
            }else if(sel==1){
                sql = "UPDATE Attendance\n" +
                        "   SET \n" +
                        "       Attendance_Time_Off = (\n" +
                        "        SELECT datetime(Attendance_Time_Off, 'localtime')\n" +
                        "        FROM Attendance\n" +
                        "        WHERE User_ID=? AND Attendance_Time_Off BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP\n" +
                        "       )\n" +
                        " WHERE User_ID=? AND Attendance_Time_Off BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP; ";
            }
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, index);
            stmt.setInt(2, index);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to Set Local Time !!!");
        }
        db.close();
        System.out.println("Local time : "+row);
        return row;
    }
    
    public int calTotal(Attendance object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Attendance\n" +
                "   SET \n" +
                "       Attendance_Total_Time = (\n" +
                "       SELECT strftime('%H', Attendance_Time_Off)-strftime('%H', Attendance_Time_Attendance)\n" +
                "        FROM Attendance\n" +
                "        WHERE User_ID=? AND Attendance_Time_Attendance BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP\n" +
                "       )\n" +
                " WHERE User_ID=? AND Attendance_Time_Attendance BETWEEN CURRENT_DATE AND CURRENT_TIMESTAMP ; ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser().getId());
            stmt.setInt(2, object.getUser().getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to cal Total !!!");
        }
        db.close();
        System.out.println("Cal Total : "+row);
        return row;
    }
    
    public static void main(String[] args) {
        AttendanceDao dao = new AttendanceDao();

        //dao.add(new Attendance(new User(1),0.5));
        //System.out.println(dao.getAll());
        System.out.println(dao.getTimeTotal(5, 11));
        //dao.get(1);
        //dao.update(new Attendance(7,0.5));
        //System.out.println(dao.add(new Customer(-1, "sumsung", "0614372734",0)));
        //System.out.println(dao.getAll());
        //System.out.println(dao.get(1));
    }

}
