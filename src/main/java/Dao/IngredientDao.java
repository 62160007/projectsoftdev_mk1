/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;
import model.Ingredient;

/**
 *
 * @author sumsung
 */
public class IngredientDao {

    public int add(Ingredient object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Ingredient (Ingredient_Name,Ingredient_Price,Ingredient_Amount,Ingredient_Unit,Ingredient_Min) VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            stmt.setString(4, object.getUnit());
            stmt.setInt(5, object.getMin());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create Ingredient.");
        }
        db.close();
        return id;
    }
    
    public ArrayList<Ingredient> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Ingredient_ID,\n"
                    + "       Ingredient_Name,\n"
                    + "       Ingredient_Price,\n"
                    + "       Ingredient_Amount,\n"
                    + "       Ingredient_Unit,\n"
                    + "       Ingredient_Min\n"
                    + "  FROM Ingredient;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Ingredient_ID");
                String name = result.getString("Ingredient_Name");
                double price = result.getDouble("Ingredient_Price");
                int amount = result.getInt("Ingredient_Amount");
                String unit = result.getString("Ingredient_Unit");
                 int min = result.getInt("Ingredient_Min");

               Ingredient ingredient = new Ingredient(id, name, price,amount,unit,min);
                list.add(ingredient);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ingredient!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }
    
    public ArrayList<Ingredient> getOutofStock() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Ingredient_ID,\n"
                    + "       Ingredient_Name,\n"
                    + "       Ingredient_Price,\n"
                    + "       Ingredient_Amount,\n"
                    + "       Ingredient_Unit,\n"
                    + "       Ingredient_Min\n"
                    + "  FROM Ingredient"
                    + "  WHERE Ingredient_Amount <= Ingredient_Min"
                    + "  ORDER BY Ingredient_Amount ASC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Ingredient_ID");
                String name = result.getString("Ingredient_Name");
                int amount = result.getInt("Ingredient_Amount");
                String unit = result.getString("Ingredient_Unit");

               Ingredient ingredient = new Ingredient(id, name,amount,unit);
                list.add(ingredient);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all ingredient!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }

    public Ingredient get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Ingredient_ID,\n"
                    + "       Ingredient_Name,\n"
                    + "       Ingredient_Price,\n"
                    + "       Ingredient_Amount,\n"
                    + "       Ingredient_Unit,\n"
                    + "       Ingredient_Min\n"
                    + "  FROM Ingredient\n"
                    + "  WHERE Ingredient_ID = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("Ingredient_ID");
                String name = result.getString("Ingredient_Name");
                double price = result.getDouble("Ingredient_Price");
                int amount = result.getInt("Ingredient_Amount");
                String unit = result.getString("Ingredient_Unit");
                int min = result.getInt("Ingredient_Min");
                Ingredient ingredient = new Ingredient(id, name, price,amount,unit,min);
                return ingredient;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select customer id " + id + "!!!");
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Ingredient\n"
                    + "      WHERE Ingredient_ID = ? ; ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect ingredient id " + id + "!!!");
        }
        db.close();
        return row;
    }

    public int update(Ingredient object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Ingredient SET Ingredient_Name = ?,Ingredient_Price = ?,Ingredient_Amount = ? ,Ingredient_Unit = ? ,Ingredient_Min = ? WHERE Ingredient_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setDouble(2, object.getPrice());
            stmt.setInt(3, object.getAmount());
            stmt.setString(4,object.getUnit());
            stmt.setInt(5,object.getMin());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to update ingredient id!!!");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        IngredientDao dao = new IngredientDao();
        //System.out.println(dao.add(new Ingredient(1,"plassatic", 300, 500 ,"pack",100 )));
        /*
        System.out.println(dao.getAll());
        System.out.println(dao.get(0));
        System.out.println(dao.delete(1));
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));*/
        System.out.println(dao.getOutofStock());

    }

}

