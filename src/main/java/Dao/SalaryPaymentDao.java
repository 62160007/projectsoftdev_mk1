/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.SalaryPayment;
import model.User;

/**
 *
 * @author sumsung
 */
public class SalaryPaymentDao{

    public int add(SalaryPayment object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Salary_Payment (User_ID,Payment_Salary,Payment_Bookbank,Payment_Date,Payment_Check)VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser().getId());
            stmt.setDouble(2, object.getSalary());
            stmt.setString(3, object.getBookbank());
            stmt.setDate(4, object.getDate());
            stmt.setString(5, object.getCheck());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create salarypayment.");
        }
        db.close();
        return id;
    }

    public ArrayList<SalaryPayment> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT sp.Payment_ID,\n"
                    + "       User_ID,\n"
                    + "       u.User_Name,\n"
                    + "       u.User_Password,\n"
                    + "       u.User_Show_Name,\n"
                    + "       u.User_Salary_Rate,\n"
                    + "       u.User_Type,\n"
                    + "       sp.Payment_Salary,\n"
                    + "       sp.Payment_Bookbank,\n"
                    + "       sp.Payment_Date,\n"
                    + "       sp.Payment_Check\n"
                    + "  FROM Salary_Payment sp ,User u"
                    + "  WHERE sp.User_ID = sp.ID;"
                    + "  ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Payment_ID");
                //FK User
                int userid = result.getInt("User_ID");
                String username = result.getString("User_Name");
                String userpassword = result.getString("User_Password");
                String usershowname = result.getString("User_Show_Name");
                double usersalaryrate= result.getDouble("User_Salary_Rate");
                String usertype = result.getString("User_Type");
                //FK User
                double salary = result.getDouble("Payment_Salary");
                String bookbank = result.getString("Payment_Bookbank");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Payment_Date"));
                String check = result.getString("Patment_Check");

                SalaryPayment salarypayment = new SalaryPayment(id, new User(userid,username,userpassword,usershowname,usersalaryrate,usertype), salary, bookbank, (java.sql.Date) date,check);
                list.add(salarypayment);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all salarypayment!!!");
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all salarypayment!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }

    public SalaryPayment get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT sp.Payment_ID,\n"
                    + "       User_ID,\n"
                    + "       u.User_Name,\n"
                    + "       u.User_Password,\n"
                    + "       u.User_Show_Name,\n"
                    + "       u.User_Salary_Rate,\n"
                    + "       u.User_Type,\n"
                    + "       sp.Payment_Salary,\n"
                    + "       sp.Payment_Bookbank,\n"
                    + "       sp.Payment_Date,\n"
                    + "       sp.Payment_Check\n"
                    + "  FROM Salary_Payment sp ,User u"
                    + "  WHERE sp.Payment_ID = ? AND sp.User_ID = sp.ID";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("Payment_ID");
                //FK User
                int userid = result.getInt("User_ID");
                String username = result.getString("User_Name");
                String userpassword = result.getString("User_Password");
                String usershowname = result.getString("User_Show_Name");
                double usersalaryrate= result.getDouble("User_Salary_Rate");
                String usertype = result.getString("User_Type");
                //FK User
                double salary = result.getDouble("Payment_Salary");
                String bookbank = result.getString("Payment_Bookbank");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Payment_Date"));
                String check = result.getString("Patment_Check");
                SalaryPayment salarypayment = new SalaryPayment(pid, new User(userid,username,userpassword,usershowname,usersalaryrate,usertype), salary, bookbank, (java.sql.Date) date,check);
                return salarypayment;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select customer id " + id + "!!!");
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all salarypayment!!!");
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Salary_Payment\n"
                    + "      WHERE Payment_ID = ? ; ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect salarypayment id " + id + "!!!");
        }
        db.close();
        return row;
    }

    /*public int update(SalaryPayment object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Ingredient SET Ingredient_ID = ?,Ingredient_Name = ?,Ingredient_Price = ?,Ingredient_Amount = ? ,Ingredient_Unit = ? ,Ingredient_Min = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getId());
            stmt.setString(2, object.getName());
            stmt.setDouble(3, object.getPrice());
            stmt.setInt(4, object.getAmount());
            stmt.setString(5, object.getUnit());
            stmt.setInt(6, object.getMin());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to update ingredient id!!!");
        }
        db.close();
        return row;
    }*/

    public static void main(String[] args) {
        SalaryPaymentDao dao = new SalaryPaymentDao();
        User u = new User("sung","sung","sumsung",100,"parttime");
        System.out.println(dao.add(new SalaryPayment(u,20000,"oo","aa")));
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        //System.out.println(dao.delete(1));
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));

    }
}
