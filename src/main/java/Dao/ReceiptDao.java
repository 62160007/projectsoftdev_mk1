/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Receipt;
import model.User;

/**
 *
 * @author sumsung
 */
public class ReceiptDao {

    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt (\n"
                    + "                        Receipt_Queue,\n"
                    + "                        User_ID,\n"
                    + "                        Customer_ID,\n"
                    + "                        Recive_Cash,\n"
                    + "                        Change_Cash,\n"
                    + "                        Discount\n"
                    + "                    ) VALUES (?, ?, ?, ?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getQueue());
            stmt.setInt(2, object.getUser().getId());
            stmt.setInt(3, object.getCustomer().getId());
            stmt.setDouble(4, object.getReciveCash());
            stmt.setDouble(5, object.getChangeCash());
            stmt.setDouble(6, object.getDiscount());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create receipt.");
        }
        db.close();
        System.out.println("id: "+id);
        //setLocalTime(id);
        return id;
    }

    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Receipt_ID,\n"
                    + "       Receipt_Queue,\n"
                    + "       Receipt_Date,\n"
                    + "       User_ID,\n"
                    + "       Customer_ID,\n"
                    + "       Recive_Cash,\n"
                    + "       Change_Cash,\n"
                    + "       Discount\n"
                    + "  FROM Receipt;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Receipt_ID");
                int queue = result.getInt("Receipt_Queue");
                Date receiptDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Receipt_Date"));
                int userId = result.getInt("User_ID");
                int customerId = result.getInt("Customer_ID");
                double reciveCash = result.getDouble("Recive_Cash");
                double changeCash = result.getDouble("Change_Cash");
                double discount = result.getDouble("Discount");

                Receipt receipt = new Receipt(id, queue, receiptDate, new User(userId), new Customer(customerId), reciveCash, changeCash, discount);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!");
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }

    public ArrayList<Receipt> getAllReceiptOrder() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Receipt_ID,\n"
                    + "       Receipt_Queue,\n"
                    + "       Receipt_Date,\n"
                    + "       User_ID,\n"
                    + "       Customer_ID,\n"
                    + "       Recive_Cash,\n"
                    + "       Change_Cash,\n"
                    + "       Discount\n"
                    + "  FROM Receipt;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Receipt_ID");
                int queue = result.getInt("Receipt_Queue");
                Date receiptDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Receipt_Date"));
                int userId = result.getInt("User_ID");
                int customerId = result.getInt("Customer_ID");
                double reciveCash = result.getDouble("Recive_Cash");
                double changeCash = result.getDouble("Change_Cash");
                double discount = result.getDouble("Discount");

                Receipt receipt = new Receipt(id, queue, receiptDate, new User(userId), new Customer(customerId), reciveCash, changeCash, discount);
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt!!!");
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }

    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Receipt_ID,\n" +
                    "       Receipt_Queue,\n" +
                    "       Receipt_Date,\n" +
                    "       User_ID,\n" +
                    "       Customer_ID,\n" +
                    "       Recive_Cash,\n" +
                    "       Change_Cash,\n" +
                    "       Discount\n" +
                    "  FROM Receipt\n" +
                    "  WHERE Receipt_ID=" + id+";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int rid = result.getInt("Receipt_ID");
                int queue = result.getInt("Receipt_Queue");
                Date receiptDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Receipt_Date"));
                int userId = result.getInt("User_ID");
                int customerId = result.getInt("Customer_ID");
                double reciveCash = result.getDouble("Recive_Cash");
                double changeCash = result.getDouble("Change_Cash");
                double discount = result.getDouble("Discount");

                Receipt receipt = new Receipt(id, queue, receiptDate, new User(userId), new Customer(customerId), reciveCash, changeCash, discount);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select receipt id " + id + "!!!");
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Receipt\n"
                    + "      WHERE Receipt_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect receipt id " + id + "!!!");
        }
        db.close();
        return row;
    }

    public int update(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Receipt\n" 
                    + "   SET Recive_Cash = ?,\n"
                    + "       Change_Cash = ?,\n"
                    + "       Discount = ?\n"
                    + " WHERE Receipt_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, object.getReciveCash());
            stmt.setDouble(2, object.getChangeCash());
            stmt.setDouble(3, object.getDiscount());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to update receipt id!!!");
        }
        db.close();
        return row;
    }
    
    public int setLocalTime(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Receipt \n" +
                "SET Receipt_Date =\n" +
                "(\n" +
                "    SELECT datetime(Receipt_Date, 'localtime')\n" +
                "    FROM Receipt\n" +
                "    WHERE Receipt_ID=?\n" +
                ")WHERE Receipt_ID=?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.setInt(2, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to Set Local Time!!!");
        }
        db.close();
        System.out.println("set Local Time : "+row);
        System.out.println("local id: "+id);
        return row;
    }
    
    public static void main(String[] args) {
        ReceiptDao dao = new ReceiptDao();
        User user = new User(1);
        Customer cus = new Customer(1);
        //System.out.println(dao.add(new Receipt(2,user,cus, 300,20,30)));
        System.out.println("............................................");
        //System.out.println(dao.getAll());
        System.out.println(dao.get(82).getDate());
        //Udao.delete(4);
        //Udao.update(new User(5, "test", "0000", "test", 999999, "test user"));
    }
}
