/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author patpunyanuch
 */
public class UserDao {

    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO User (User_Name, User_Password, User_Show_Name, User_Salary_Rate, User_Type) VALUES (?, ?, ?, ?, ?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getShowName());
            stmt.setDouble(4, object.getSalaryRate());
            stmt.setString(5, object.getType());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create user.");
        }
        db.close();
        return id;
    }

    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT User_ID, User_Name, User_Password, User_Show_Name, User_Salary_Rate, User_Type FROM User;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("User_ID");
                String name = result.getString("User_Name");
                String password = result.getString("User_Password");
                String showName = result.getString("User_Show_Name");
                double salaryRate = result.getDouble("User_Salary_Rate");
                String type = result.getString("User_Type");

                User user = new User(id, name, password, showName, salaryRate, type);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all user!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }

    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT User_ID, User_Name, User_Password, User_Show_Name, User_Salary_Rate, User_Type FROM User WHERE User_ID = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("User_ID");
                String name = result.getString("User_Name");
                String password = result.getString("User_Password");
                String showName = result.getString("User_Show_Name");
                double salaryRate = result.getDouble("User_Salary_Rate");
                String type = result.getString("User_Type");
                User user = new User(id, name, password, showName, salaryRate, type);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select user id " + id + "!!!");
        }
        db.close();
        return null;
    }

    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM User WHERE User_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect user id " + id + "!!!");
        }
        db.close();
        return row;
    }

    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE User SET User_Name = ?, User_Password = ?, User_Show_Name = ?, User_Salary_Rate = ?, User_Type = ? WHERE User_ID = ?;";
            //String sql = "UPDATE User SET User_ID = ?, User_Name = ?, User_Password = ?, User_Show_Name = ?, User_Salary_Rate = ?, User_Type = ? WHERE User_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            //stmt.setInt(1, object.getId());
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getShowName());
            stmt.setDouble(4, object.getSalaryRate());
            stmt.setString(5, object.getType());
            stmt.setInt(6, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to update user id!!!");
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao Udao = new UserDao();
        //System.out.println(Udao.add(new User("admin","0000","admin", 999999, "super user")));
        System.out.println(Udao.getAll());
        System.out.println(Udao.get(1));
        //Udao.delete(4);
        Udao.update(new User(5,"test","0000","test", 999999, "test user"));
    }

}
