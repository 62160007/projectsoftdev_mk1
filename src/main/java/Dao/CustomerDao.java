/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author sumsung
 */
public class CustomerDao {

    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Customer (Customer_Name,Customer_Phone,Customer_Come) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getCome());

            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            System.out.println("Error : to create customer.");
        }
        db.close();
        return id;
    }

    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Customer_ID,\n"
                    + "       Customer_Name,\n"
                    + "       Customer_Phone,\n"
                    + "       Customer_Come\n"
                    + "  FROM Customer;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Customer_ID");
                String name = result.getString("Customer_Name");
                String tel = result.getString("Customer_Phone");
                int come = result.getInt("Customer_Come");

                Customer customer = new Customer(id, name, tel, come);
                list.add(customer);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all customer!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");
        }
        db.close();
        return list;
    }

    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Customer_ID,\n" +
"       Customer_Name,\n" +
"       Customer_Phone,\n" +
"       Customer_Come\n" +
"  FROM Customer\n" +
"  WHERE Customer_ID = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int pid = result.getInt("Customer_ID");
                String name = result.getString("Customer_Name");
                String tel = result.getString("Customer_Phone");
                int come = result.getInt("Customer_Come");
                Customer customer = new Customer(id, name, tel, come);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select customer id " + id + "!!!");
        }
        db.close();
        return null;
    }
    
    public Customer getPhone(String phone) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Customer_ID,\n" +
                "       Customer_Name,\n" +
                "       Customer_Phone,\n" +
                "       Customer_Come\n" +
                "  FROM Customer\n" +
                "  WHERE Customer_Phone = \"" + phone+"\";";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int cid = result.getInt("Customer_ID");
                String name = result.getString("Customer_Name");
                String tel = result.getString("Customer_Phone");
                int come = result.getInt("Customer_Come");
                Customer customer = new Customer(cid, name, tel, come);
                return customer;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select customer phone " + phone + "!!!");
        }
        db.close();
        return null;
    }
    
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Customer\n" +
"      WHERE Customer_ID = ?; ";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delect customer id " + id + "!!!");
        }
        db.close();
        return row;
    }

    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Customer SET Customer_Name = ?,Customer_Phone = ?,Customer_Come = ? WHERE Customer_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getPhone());
            stmt.setInt(3, object.getCome());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to update customer id!!!");
        }
        db.close();
        return row;
    }
    
    public int plusCount(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Customer SET Customer_Come = ? WHERE Customer_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            CustomerDao cusDao = new CustomerDao();
            stmt.setInt(1, (cusDao.get(object.getId()).getCome()+1));
            stmt.setInt(2, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to plus count!!!");
        }
        db.close();
        return row;
    }
    
    public int minusCount(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        CustomerDao cusDao = new CustomerDao();
        try {
            if(cusDao.get(object.getId()).getCome()-1<0){
                return -1;
            }
            String sql = "UPDATE Customer SET Customer_Come = ? WHERE Customer_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, (cusDao.get(object.getId()).getCome()-1));
            stmt.setInt(2, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Error : Unable to plus count!!!");
        }
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        //System.out.println(dao.add(new Customer(-1, "sumsung", "0614372734",0)));
        //System.out.println(dao.getAll());
        //System.out.println(dao.get(1));
        System.out.println(dao.getPhone("0885554444"));
        System.out.println(dao.plusCount(dao.get(5)));
        System.out.println(dao.minusCount(dao.get(5)));
    }

}
