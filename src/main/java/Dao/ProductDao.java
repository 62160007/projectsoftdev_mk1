/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author patpunyanuch
 */
public class ProductDao {
    

    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Product_ID, Product_Name, Product_Price, Product_Image, Product_Type FROM Product;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("Product_ID");
                String name = result.getString("Product_Name");
                double price = result.getDouble("Product_Price");
                String img = result.getString("Product_Image");
                String type = result.getString("Product_Type");

                Product p = new Product(id, name, price, img, type);
                list.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all Product!!!");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database ");

        }
        db.close();
        return list;
    }
    
    public static void main(String[] args) {
        ArrayList<Product> productList = new ArrayList<>();
        ProductDao dao = new ProductDao();
        productList = dao.getAll();
        System.out.println(productList);
    }

}
