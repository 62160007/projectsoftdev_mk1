/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.CustomerDao;
import Dao.ReceiptDao;
import Dao.ReceiptDetailDao;
import java.awt.Component;
import java.awt.HeadlessException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import model.Customer;
import model.Ingredient;
import model.Receipt;
import model.ReceiptOrderTable;
import model.User;

/**
 *
 * @author sumsung
 */
public class OrderManagementPanel extends javax.swing.JPanel {

    private final Main mainFrame;
    private ArrayList<Receipt> receiptList;
    private ReceiptOrderTable model;
    Receipt editrReceiptOrder;
    private ReceiptDao dao = new ReceiptDao();
    private CustomerDao customerDao = new CustomerDao();
    private int index = -1;
    private ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();

    /**
     * Creates new form UserPanel
     */
    public OrderManagementPanel(Main mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        showOrderTable();
        //editPanelVisible(false);
        tblOrderManagement.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                editrReceiptOrder = receiptList.get(tblOrderManagement.getSelectedRow());
                System.out.println(receiptList.get(tblOrderManagement.getSelectedRow()));
                loadReceiptOrderToForm();
            }
        });

    }

    public void loadReceiptOrderToForm() {
        txtOrderCode.setText("" + editrReceiptOrder.getId());
        lblShowQueue.setText("" + editrReceiptOrder.getQueue());
        lblShowDate.setText("" + editrReceiptOrder.getDate());
        lblShowUserID.setText("" + editrReceiptOrder.getUser().getId());
        lblShowCustomerID.setText("" + editrReceiptOrder.getCustomer().getId());
        txtTotal.setText(""+editrReceiptOrder.getTotal());
        edtReciveCash.setText("" + editrReceiptOrder.getReciveCash());
        edtChangeCash.setText("" + editrReceiptOrder.getChangeCash());
        edtDiscount.setText("" + editrReceiptOrder.getDiscount());

    }

    public void clearData() {
        //editPanelVisible(false);
        txtOrderCode.setText("-1");
        lblShowQueue.setText("");
        lblShowDate.setText("");
        lblShowUserID.setText("");
        lblShowCustomerID.setText("");
        txtTotal.setText("");
        edtReciveCash.setText("");
        edtChangeCash.setText("");
        edtDiscount.setText("");
    }

    public void editPanelVisible(boolean isVisible) {
        pnlReceiptOrder.setVisible(isVisible);
    }

    public void confirmYN(String conMessage) throws HeadlessException {
        int reply = JOptionPane.showConfirmDialog(null, "Are you sure to delete"
                + " " + conMessage + " ReceiptOrder.", "Warning!",
                JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            //customerDao.minusCount(dao.get(Integer.parseInt(txtOrderCode.getText())).getCustomer());
            dao.delete(Integer.parseInt(txtOrderCode.getText()));
            receiptDetailDao.deleteAllRecieptDetail(Integer.parseInt(txtOrderCode.getText()));
            refreshTableReceiptOrder();
            JOptionPane.showMessageDialog(null, "Ingredient " + conMessage
                    + " has removed.");
        } else {
            JOptionPane.showMessageDialog(null, "Delete process has cancel.");
        }
    }

    public void showMessage(String message) {
        Component frame = null;
        JOptionPane.showMessageDialog(frame, message);
    }

    private void refreshTableReceiptOrder() {
        ArrayList<Receipt> newList = dao.getAll();
        receiptList.clear();
        receiptList.addAll(newList);
        tblOrderManagement.invalidate();
        tblOrderManagement.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        umDelete = new javax.swing.JButton();
        umEdit = new javax.swing.JButton();
        tableOrder = new javax.swing.JScrollPane();
        tblOrderManagement = new javax.swing.JTable();
        pnlReceiptOrder = new javax.swing.JPanel();
        lblOrderCode = new javax.swing.JLabel();
        lblQueue = new javax.swing.JLabel();
        lbllDate = new javax.swing.JLabel();
        lblCustomerID = new javax.swing.JLabel();
        lblTotalPrice = new javax.swing.JLabel();
        txtOrderCode = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lbllStaffID = new javax.swing.JLabel();
        lblReciveCash = new javax.swing.JLabel();
        lblCash = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        edtReciveCash = new javax.swing.JTextField();
        edtDiscount = new javax.swing.JTextField();
        edtChangeCash = new javax.swing.JTextField();
        txtTotal = new javax.swing.JLabel();
        lblShowUserID = new javax.swing.JLabel();
        lblShowQueue = new javax.swing.JLabel();
        lblShowDate = new javax.swing.JLabel();
        lblShowCustomerID = new javax.swing.JLabel();

        jScrollPane2.setViewportView(jTextPane1);

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(821, 572));

        umDelete.setBackground(new java.awt.Color(255, 102, 102));
        umDelete.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        umDelete.setText("Delete");
        umDelete.setPreferredSize(new java.awt.Dimension(75, 27));
        umDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                umDeleteActionPerformed(evt);
            }
        });

        umEdit.setBackground(new java.awt.Color(204, 255, 204));
        umEdit.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        umEdit.setText("Edit");
        umEdit.setPreferredSize(new java.awt.Dimension(75, 27));
        umEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                umEditActionPerformed(evt);
            }
        });

        tableOrder.setPreferredSize(new java.awt.Dimension(500, 500));

        tblOrderManagement.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order Code", "Queue", "Date", "Staff ID", "Customer ID", "Total Price(Bath)", "Recive Cash (Bath)", "Change (Bath)", "Discount(Bath)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblOrderManagement.setToolTipText("");
        tblOrderManagement.setColumnSelectionAllowed(true);
        tblOrderManagement.setRowHeight(40);
        tableOrder.setViewportView(tblOrderManagement);
        tblOrderManagement.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (tblOrderManagement.getColumnModel().getColumnCount() > 0) {
            tblOrderManagement.getColumnModel().getColumn(0).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(1).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(2).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(3).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(4).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(5).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(6).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(7).setResizable(false);
            tblOrderManagement.getColumnModel().getColumn(8).setResizable(false);
        }

        pnlReceiptOrder.setBackground(new java.awt.Color(255, 255, 255));

        lblOrderCode.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblOrderCode.setText("Order Code :");

        lblQueue.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblQueue.setText("Queue :");

        lbllDate.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lbllDate.setText("Date :");

        lblCustomerID.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblCustomerID.setText("Customer ID :");

        lblTotalPrice.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblTotalPrice.setText("Total Price :");

        btnSave.setBackground(new java.awt.Color(204, 255, 204));
        btnSave.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnSave.setText("Save");
        btnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnSave.setPreferredSize(new java.awt.Dimension(75, 27));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 102, 102));
        btnCancel.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnCancel.setPreferredSize(new java.awt.Dimension(75, 27));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lbllStaffID.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lbllStaffID.setText("Staff ID :");

        lblReciveCash.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblReciveCash.setText("Recive Cash :");

        lblCash.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblCash.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCash.setText("Change Cash :");

        lblDiscount.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("Discount :");

        edtReciveCash.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        edtReciveCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtReciveCashActionPerformed(evt);
            }
        });

        edtDiscount.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        edtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtDiscountActionPerformed(evt);
            }
        });

        edtChangeCash.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        edtChangeCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtChangeCashActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlReceiptOrderLayout = new javax.swing.GroupLayout(pnlReceiptOrder);
        pnlReceiptOrder.setLayout(pnlReceiptOrderLayout);
        pnlReceiptOrderLayout.setHorizontalGroup(
            pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbllDate)
                            .addComponent(lblQueue)
                            .addComponent(lblOrderCode)
                            .addComponent(lbllStaffID))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtOrderCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblShowUserID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblShowQueue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblShowDate, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                        .addComponent(lblCustomerID)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblShowCustomerID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                                .addComponent(lblTotalPrice)
                                .addGap(15, 15, 15)
                                .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lblCash, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblReciveCash, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblDiscount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtReciveCash, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(edtChangeCash, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlReceiptOrderLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlReceiptOrderLayout.setVerticalGroup(
            pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(lblTotalPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblReciveCash, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edtReciveCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCash)
                            .addComponent(edtChangeCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtOrderCode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblOrderCode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(lblShowQueue, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlReceiptOrderLayout.createSequentialGroup()
                                .addComponent(lblQueue, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lbllDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblShowDate, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lbllStaffID, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblShowUserID, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pnlReceiptOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblCustomerID)
                                    .addComponent(lblShowCustomerID, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlReceiptOrder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(634, 634, 634)
                        .addComponent(umEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(umDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(tableOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(57, 57, 57))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(pnlReceiptOrder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(umEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(umDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tableOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        //editPanelVisible(false);
        refreshTableReceiptOrder();
        
        System.out.println(Integer.parseInt(txtOrderCode.getText()));
        System.out.println("index : "+index);/*
        System.out.println(Integer.parseInt(lblShowQueue.getText()));
        System.out.println(lblShowDate);
        System.out.println(new User(Integer.parseInt(lblShowUserID.getText())));
        System.out.println(new Customer(Integer.parseInt(lblShowCustomerID.getText())));
        System.out.println(Double.parseDouble(edtReciveCash.getText()));
        System.out.println(Double.parseDouble(edtChangeCash.getText()));
        System.out.println(Double.parseDouble(edtDiscount.getText()));*/
        Receipt receiptData = new Receipt(Integer.parseInt(txtOrderCode.getText()),Integer.parseInt(lblShowQueue.getText()), new User(Integer.parseInt(lblShowUserID.getText())), new Customer(Integer.parseInt(lblShowCustomerID.getText())), Double.parseDouble(edtReciveCash.getText()),Double.parseDouble(edtChangeCash.getText()),Double.parseDouble(edtDiscount.getText()));
        System.out.println(receiptData);
        
        //if (index >= 0) {
            int row = dao.update(receiptData);
            refreshTableReceiptOrder();
            if (row > 0) {
                showMessage("Update ReceiptOrder ID : " + txtOrderCode.getText() + " is complete.");
                System.out.println(row);
            } else {
                showMessage("Insert ReceiptOrder ID : " + txtOrderCode.getText() + " fail.");
            }
        //} 
        refreshTableReceiptOrder();
        
        System.out.println("Complete!");
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearData();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void edtReciveCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtReciveCashActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtReciveCashActionPerformed

    private void edtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtDiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtDiscountActionPerformed

    private void edtChangeCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtChangeCashActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_edtChangeCashActionPerformed

    private void umEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_umEditActionPerformed
        if (txtOrderCode.getText().equals("")) {
            showMessage("Please select data.");
            return;
        }
        editPanelVisible(true);
        index=Integer.parseInt(txtOrderCode.getText());
    }//GEN-LAST:event_umEditActionPerformed

    private void umDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_umDeleteActionPerformed
         //editPanelVisible(true);
        if (txtOrderCode.getText().equals("")) {
            showMessage("Please select data.");
            return;
        }
        refreshTableReceiptOrder();
        confirmYN(txtOrderCode.getText());
        refreshTableReceiptOrder();
        clearData();                        

    }//GEN-LAST:event_umDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtChangeCash;
    private javax.swing.JTextField edtDiscount;
    private javax.swing.JTextField edtReciveCash;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblCustomerID;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblOrderCode;
    private javax.swing.JLabel lblQueue;
    private javax.swing.JLabel lblReciveCash;
    private javax.swing.JLabel lblShowCustomerID;
    private javax.swing.JLabel lblShowDate;
    private javax.swing.JLabel lblShowQueue;
    private javax.swing.JLabel lblShowUserID;
    private javax.swing.JLabel lblTotalPrice;
    private javax.swing.JLabel lbllDate;
    private javax.swing.JLabel lbllStaffID;
    private javax.swing.JPanel pnlReceiptOrder;
    private javax.swing.JScrollPane tableOrder;
    private javax.swing.JTable tblOrderManagement;
    private javax.swing.JLabel txtOrderCode;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JButton umDelete;
    private javax.swing.JButton umEdit;
    // End of variables declaration//GEN-END:variables

    private void showOrderTable() {
        receiptList = dao.getAllReceiptOrder();
        model = new ReceiptOrderTable(receiptList);
        tblOrderManagement.setModel((TableModel) model);
    }
}
