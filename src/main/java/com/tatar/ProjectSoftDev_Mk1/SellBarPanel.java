/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.CustomerDao;
import Dao.ReceiptDao;
import Dao.ReceiptDetailDao;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import model.Customer;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author sumsung
 */
public class SellBarPanel extends javax.swing.JPanel {
        private final Main mainFrame;
        private Customer customer;
        private CustomerDao customerDao = new CustomerDao();
        private Receipt receipt;
        private ReceiptDetail receiptDetail;
        private ReceiptDao receiptDao = new ReceiptDao();
        private ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        private double sum=0;
        private double discount=0;
        private double total=0;
        private double change=0;
        private PosPanel pp;
        private int totalQty=0;
        //private int queue=0;
        //private ArrayList<ReceiptDetail> receiptDetailList=null;
    /**
     * Creates new form sellBarPanel
     */
    public SellBarPanel(Main mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        this.customer=new Customer(-1,"Non customer","-",0);
        mainFrame.switchPosPanel(this);
        reCal();
        refreshCal();
    }
    public void refreshCal(){
        editChange.setText(""+change);
        editDiscount.setText(""+discount);
        editSum.setText(""+sum);
        editTotal.setText(""+total);
        editMemberName.setText(customer.getName());
    }
    public void reCal(){
        sum=CartService.getSumCart();
        if(customer.getId()>=0){
            discount=10;
        }else{
            discount=0;
        }
        total=0 ;
        total=sum-discount;
        change=Double.parseDouble(editReciveCash.getText())-total;              
    }
    
    public void showReceipt(){
        try{
        Component frame = null;
		String message="";
		int queue=receipt.getQueue();
		String phone= "0387548965";
		String billID = ""+receipt.getId();
		String user = receipt.getUser().getShowName();
                String customer = this.customer.getName();
		String date = ""+receiptDao.get(receipt.getId()).getDate();
		String spaceProduct = "                       ";
		String spaceQty="       ";
                String line="\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
                String spaceLong = "                                      ";
                
                
		ArrayList<ReceiptDetail> receiptDetailList = new ArrayList<>();                
                for(int i=0;i<CartService.getCart().size();i++){
                    receiptDetail = new ReceiptDetail(receipt,CartService.getCart().get(i).getProduct(),CartService.getCart().get(i).getAmount(),CartService.getCart().get(i).getPrice(),(CartService.getCart().get(i).getAmount()*CartService.getCart().get(i).getPrice()));
                    //receiptDetailList.add(receiptDetail);
                    //rows+={receiptDetail.getProduct().getName(),""+receiptDetail.getAmount(),""+receiptDetail.getPriceTotal()}
                    receiptDetailList.add(receiptDetail);
                }
                
                String receiptDetailData = createReceiptDetailData(receiptDetailList);
                
		message =spaceQty+"             Queue No: "+queue+"\n"+spaceQty+"            Inthanin coffee\n"+spaceQty+" Branch: Burapha University"+"\n"+spaceQty+"     Phone: "+phone+"\n"+spaceProduct+"  Receipt"+line
		+"\nBill ID: "+billID+"\nSeller: "+user+"\nCustomer name: "+customer+"\nDate: "+date+line+"\nProduct"+spaceProduct+"Qty"+spaceQty+"Total"+"\n\n"+receiptDetailData;
        JOptionPane.showMessageDialog(frame, message);
        }catch(Exception e){
            Component frame = null;
            JOptionPane.showMessageDialog(frame, "Please Cash before print receipt.");
        }
    }
    
    public String createReceiptDetailData(ArrayList<ReceiptDetail> receiptDetailList){
        totalQty=0;
        String receiptDetailData="";
        String line="\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - -";
        //ArrayList<ReceiptDetail> receiptDetailList = new ArrayList<>();
        for(int i=0;i<receiptDetailList.size();i++){
            totalQty+=receiptDetailList.get(i).getAmount();
            int presize = 30;
            receiptDetailData+=receiptDetailList.get(i).getProduct().getName();
            receiptDetailData+="\n";
            for(int j=0;j<presize;j++){
                receiptDetailData+=" ";
            }
            receiptDetailData+="        ";
            receiptDetailData+=receiptDetailList.get(i).getAmount();
            receiptDetailData+="       ";
            receiptDetailData+=freeSpace(15-(""+receiptDetailList.get(i).getPriceTotal()).length());
            receiptDetailData+=receiptDetailList.get(i).getPriceAmount();
            receiptDetailData+="\n";
        }
        receiptDetailData+="\n";
        String totalamount=""+totalQty;
        String totalSum=editSum.getText();
        String totalPrice=editTotal.getText();
        String totalDiscount=editDiscount.getText();
        String totalChange=editChange.getText();
        String totalReceive=editReciveCash.getText();
        receiptDetailData+="Sub Total:";
        receiptDetailData+=freeSpace(20);  
        receiptDetailData+=totalQty;
        receiptDetailData+="      ";
        receiptDetailData+=freeSpace(15-totalSum.length());
        receiptDetailData+=totalSum+"\n"+line+"\nDiscount:";
        receiptDetailData+=freeSpace(31);        
        receiptDetailData+=""+freeSpace(15-totalDiscount.length())+totalDiscount+"\nTotal:";
        receiptDetailData+=freeSpace(38);
        receiptDetailData+=""+freeSpace(15-totalPrice.length())+totalPrice+"\nReceive Cash:";
        receiptDetailData+=freeSpace(22);
        receiptDetailData+=""+freeSpace(15-totalReceive.length())+totalReceive+"\nChange:";
        receiptDetailData+=freeSpace(33);
        receiptDetailData+=""+freeSpace(15-totalChange.length())+totalChange+"\n";
        receiptDetailData+=freeSpace(22)+"-Thank You-";
        return receiptDetailData;
    }
    public String freeSpace(int index){
        String dataOut="";
        for(int j=0;j<index;j++){
            dataOut+=" ";
        }
        return dataOut;
    }
    public void clearMember(){
        this.customer=new Customer(-1,"Non customer","-",0);
        reCal();
        refreshCal();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel7 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();
        btnCash = new javax.swing.JButton();
        lblDiscount = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        btnSearchMem = new javax.swing.JButton();
        btnSubscription = new javax.swing.JButton();
        editDiscount = new javax.swing.JLabel();
        editTotal = new javax.swing.JLabel();
        editChange = new javax.swing.JLabel();
        editReciveCash = new javax.swing.JTextField();
        lblBath = new javax.swing.JLabel();
        lblBath2 = new javax.swing.JLabel();
        lblBath3 = new javax.swing.JLabel();
        lblBath6 = new javax.swing.JLabel();
        lblSum = new javax.swing.JLabel();
        editSum = new javax.swing.JLabel();
        lblBath7 = new javax.swing.JLabel();
        lblMember = new javax.swing.JLabel();
        editMemberName = new javax.swing.JLabel();

        jLabel7.setText("jLabel7");

        setBackground(new java.awt.Color(255, 255, 255));

        btnBack.setBackground(new java.awt.Color(204, 204, 255));
        btnBack.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 102, 102));
        btnCancel.setFont(new java.awt.Font("Courier New", 1, 10)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setPreferredSize(new java.awt.Dimension(70, 30));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnPrint.setBackground(new java.awt.Color(204, 255, 204));
        btnPrint.setFont(new java.awt.Font("Courier New", 1, 10)); // NOI18N
        btnPrint.setText("Print");
        btnPrint.setPreferredSize(new java.awt.Dimension(70, 35));
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnCash.setBackground(new java.awt.Color(204, 255, 204));
        btnCash.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        btnCash.setText("Cash");
        btnCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCashActionPerformed(evt);
            }
        });

        lblDiscount.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblDiscount.setText("DISCOUNT : ");

        lblTotal.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblTotal.setText("TOTAL : ");

        jLabel5.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        jLabel5.setText("RECEIVE CASH :");

        lblChange.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblChange.setText("CHANGE : ");

        btnSearchMem.setBackground(new java.awt.Color(204, 204, 255));
        btnSearchMem.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        btnSearchMem.setText("Search Member");
        btnSearchMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMemActionPerformed(evt);
            }
        });

        btnSubscription.setBackground(new java.awt.Color(204, 204, 255));
        btnSubscription.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        btnSubscription.setText("Subscription");
        btnSubscription.setPreferredSize(new java.awt.Dimension(170, 35));
        btnSubscription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubscriptionActionPerformed(evt);
            }
        });

        editDiscount.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        editDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        editDiscount.setText("0.0");
        editDiscount.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        editTotal.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        editTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        editTotal.setText("0.0");
        editTotal.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        editChange.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        editChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        editChange.setText("0.0");
        editChange.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        editReciveCash.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        editReciveCash.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        editReciveCash.setText("0");
        editReciveCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editReciveCashActionPerformed(evt);
            }
        });

        lblBath.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblBath.setText("฿");

        lblBath2.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblBath2.setText("฿");

        lblBath3.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblBath3.setText("฿");

        lblBath6.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblBath6.setText("฿");

        lblSum.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblSum.setText("SUM : ");

        editSum.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        editSum.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        editSum.setText("0.0");
        editSum.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lblBath7.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblBath7.setText("฿");

        lblMember.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        lblMember.setText("MEMBER : ");

        editMemberName.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        editMemberName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        editMemberName.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(btnSubscription, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSearchMem, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(9, 9, 9)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnCash, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblSum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(26, 26, 26)
                                .addComponent(editSum, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblBath7))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblChange, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(editTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(editReciveCash, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(editChange, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(6, 6, 6)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblBath2)
                                            .addComponent(lblBath3)
                                            .addComponent(lblBath)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(editDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lblBath6))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblMember)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(editMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSearchMem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSubscription, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblMember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSum)
                    .addComponent(editSum)
                    .addComponent(lblBath7))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDiscount)
                    .addComponent(editDiscount)
                    .addComponent(lblBath6))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotal)
                    .addComponent(editTotal)
                    .addComponent(lblBath3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(editReciveCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBath2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChange)
                    .addComponent(editChange)
                    .addComponent(lblBath))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(btnCash, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubscriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubscriptionActionPerformed
        JTextField phone = new JTextField();
	JTextField name = new JTextField();
	Object[] message = {
            "Phone :", phone,
            "Name:", name
        };

	int option = JOptionPane.showConfirmDialog(null, message, "Subscription", JOptionPane.OK_CANCEL_OPTION);
        Component frame = null;
	if (option == JOptionPane.OK_OPTION) {
            customer = new Customer(name.getText(),phone.getText(),0);
            int row = customerDao.add(customer);
            customer = customerDao.get(row);
            if (row>=0) {
                JOptionPane.showMessageDialog(frame, "Subscription Customer "+customer.getName()+" successful.");
                System.out.println("Subscription successful.");
                reCal();
                refreshCal();
            } else {
                JOptionPane.showMessageDialog(frame, "Subscription fail.");
                System.out.println("Subscription failed.");
            }
	} else {
            JOptionPane.showMessageDialog(frame, "Subscription cancel.");
            System.out.println("Subscription cancel.");
        }
        reCal();
        refreshCal();
    }//GEN-LAST:event_btnSubscriptionActionPerformed

    private void btnSearchMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMemActionPerformed
        JTextField phone = new JTextField();
	Object[] message = {
            "Phone :", phone,
        };
	int option = JOptionPane.showConfirmDialog(null, message, "Subscription", JOptionPane.OK_CANCEL_OPTION);
        Component frame = null;
	if (option == JOptionPane.OK_OPTION) { 
            customer=customerDao.getPhone(phone.getText());
            System.out.println(customerDao.getPhone(phone.getText()));
            if (customer!=null) {
                JOptionPane.showMessageDialog(frame, "Member name "+customer.getName()+" find.");
                System.out.println("Find member successful.");
                reCal();
                refreshCal();
            } else {
                JOptionPane.showMessageDialog(frame, "can't find member.");
                System.out.println("can't find member.");
            }
	} else {
            JOptionPane.showMessageDialog(frame, "Subscription cancel.");
            System.out.println("Find member cancel.");
        }
        reCal();
        refreshCal();
    }//GEN-LAST:event_btnSearchMemActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        mainFrame.backToMainFromPos();
        mainFrame.showTitle("Main Menu");
    }//GEN-LAST:event_btnBackActionPerformed

    private void editReciveCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editReciveCashActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editReciveCashActionPerformed

    private void btnCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCashActionPerformed
        reCal();
        refreshCal();
        Component frame = null;
        if(change>=0&&CartService.getCart().size()>0){
            JOptionPane.showMessageDialog(frame, "Change: "+change+" ฿");
            receipt = new Receipt(mainFrame.getCount(),null,UserService.getCurrentUser(),customer,Double.parseDouble(editReciveCash.getText()),Double.parseDouble(editChange.getText()),Double.parseDouble(editDiscount.getText()));
            int receiptID = receiptDao.add(receipt);
            receiptDao.setLocalTime(receiptID);
            int receiptDetailID=-1;
            for(int i=0;i<CartService.getCart().size();i++){
                receiptDetail = new ReceiptDetail(receipt,CartService.getCart().get(i).getProduct(),CartService.getCart().get(i).getAmount(),CartService.getCart().get(i).getPrice(),(CartService.getCart().get(i).getAmount()*CartService.getCart().get(i).getPrice()));
                //receiptDetailList.add(receiptDetail);
                receiptDetailID=receiptDetailDao.add(receiptDetail);
            }if(customer.getId()>=0){
                customerDao.plusCount(customer);
            }
            mainFrame.plusCount();
            showReceipt();
        }else if(CartService.getCart().size()==0){
            JOptionPane.showMessageDialog(frame, "Please select order.");
        }else{
            JOptionPane.showMessageDialog(frame, "Not enough money.");
        }     
    }//GEN-LAST:event_btnCashActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        /*receipt = new Receipt(mainFrame.getCount(),null,UserService.getCurrentUser(),customer,Double.parseDouble(editReciveCash.getText()),Double.parseDouble(editChange.getText()),Double.parseDouble(editDiscount.getText()));
        for(int i=0;i<CartService.getCart().size();i++){
            receiptDetail = new ReceiptDetail(receipt,CartService.getCart().get(i).getProduct(),CartService.getCart().get(i).getAmount(),CartService.getCart().get(i).getPrice(),(CartService.getCart().get(i).getAmount()*CartService.getCart().get(i).getPrice()));
        }*/
        showReceipt();
        //this.queue++;
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearMember();
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCash;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSearchMem;
    private javax.swing.JButton btnSubscription;
    private javax.swing.JLabel editChange;
    private javax.swing.JLabel editDiscount;
    private javax.swing.JLabel editMemberName;
    private javax.swing.JTextField editReciveCash;
    private javax.swing.JLabel editSum;
    private javax.swing.JLabel editTotal;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lblBath;
    private javax.swing.JLabel lblBath2;
    private javax.swing.JLabel lblBath3;
    private javax.swing.JLabel lblBath6;
    private javax.swing.JLabel lblBath7;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMember;
    private javax.swing.JLabel lblSum;
    private javax.swing.JLabel lblTotal;
    // End of variables declaration//GEN-END:variables
}
