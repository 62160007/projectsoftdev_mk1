/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import java.util.ArrayList;
import model.Cart;

/**
 *
 * @author patpunyanuch
 */
public class CartService {

    private static ArrayList<Cart> cartList = new ArrayList<>();

    public static void addCart(Cart cart) {
        for (int i = 0; i < cartList.size(); i++) {
            if (cartList.get(i).getName().equals(cart.getName())) {
                if (cart.getAmount() == 0) {
                    cartList.remove(i);
                    return;
                }
                cartList.get(i).setAmount(cart.getAmount());
                cartList.get(i).setPrice(cart.getPrice());
                return;
            }
        }
        if (cart.getAmount() == 0) {
            return;
        }
        cartList.add(cart);
    }
    
    public static double getSumCart(){
        double sum = 0;
        for(Cart c : cartList){
            sum+=c.getPrice();
        }
        return sum;
    }

    public static ArrayList<Cart> getCart() {
        return cartList;
    }
    
    public static void delCart(int index){
        cartList.remove(index);
    }
    
    public static void delAllCart(){
        cartList.clear();
    }
}
