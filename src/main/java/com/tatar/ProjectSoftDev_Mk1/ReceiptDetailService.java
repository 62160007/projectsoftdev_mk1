/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.ReceiptDetailDao;
import java.util.ArrayList;
import model.CumulativeSales;

/**
 *
 * @author tatar
 */
public class ReceiptDetailService {
    private static ReceiptDetailDao dao = new ReceiptDetailDao();
    
    public static ArrayList<CumulativeSales> getBest(){
        ArrayList<CumulativeSales> bestSell=dao.getBestSell();
        return bestSell;
    }
    
    public static ArrayList<CumulativeSales> getWorst(){
        ArrayList<CumulativeSales> worstSell=dao.getWorstSell();
        return worstSell;
    }
}
