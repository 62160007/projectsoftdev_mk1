/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.UserDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
/**
 *
 * @author tatar
 */
public class UserService {
    
// note : not complete wait for edit user data in database
    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;
    private static User superAdmin = new User("root","0000","Super User",9999999,"Fulltime");
    private static UserDao userDB = new UserDao();
    static {
        //userList = new ArrayList<>();
        // Mock Data
        //userList.add(superAdmin);
        load();
    }

    // Create user
    public static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }

    // Delete user
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }

    // Read user
    public static ArrayList<User> getUsers() {
        return userList;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }

    // Update user
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }
    
    // File 
    public static void save() {
        /*
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos=new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
            System.out.println();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }*/
    }

    public static void load() {/*
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis=new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        userList.clear();
        userList=userDB.getAll();
        //System.out.println(userList);
    }
    public static User logIn(String userName,String password){
        load();
        if(userName.equals(superAdmin.getName())&&password.equals(superAdmin.getPassword())){
            currentUser=superAdmin;
            return superAdmin;
        }
        for(int i=0;i<userList.size();i++){
            User user = userList.get(i);
            if(user.getName().equals(userName)&&user.getPassword().equals(password)){
                currentUser=user;
                return user;
            }
        }
        return null;
    }
    
    public static User logInTime(String userName,String password){
        load();
        if(userName.equals(superAdmin.getName())&&password.equals(superAdmin.getPassword())){
            return superAdmin;
        }
        for(int i=0;i<userList.size();i++){
            User user = userList.get(i);
            if(user.getName().equals(userName)&&user.getPassword().equals(password)){
                return user;
            }
        }
        return null;
    }
    
    public static boolean isLogin(){
        return currentUser !=null;
    }
    public static User getCurrentUser(){
        return currentUser;
    }
    
    public static void logout(){
        currentUser = null;
    }
}
