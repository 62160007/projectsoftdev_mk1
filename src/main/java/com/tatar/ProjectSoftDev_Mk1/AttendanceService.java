/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.AttendanceDao;
import Dao.UserDao;
import static com.tatar.ProjectSoftDev_Mk1.UserService.save;
import java.util.ArrayList;
import model.Attendance;

/**
 *
 * @author sumsung
 */
public class AttendanceService {

    private static ArrayList<Attendance> attendanceList = new ArrayList<>();
    private static AttendanceDao attendanceDB = new AttendanceDao();
    
    public static boolean addAttendance(Attendance user) {
        attendanceList.add(user);
        save();
        return true;
    }

    // Delete user
    public static boolean delAttendance(Attendance user) {
        attendanceList.remove(user);
        save();
        return true;
    }

    public static boolean delAttendance(int index) {
        attendanceList.remove(index);
        save();
        return true;
    }

    // Read user
    public static ArrayList<Attendance> getAttendance() {
        return attendanceList;
    }

    public static Attendance getAttendance(int index) {
        return attendanceList.get(index);
    }

    // Update user
    public static boolean updateAttendance(int index, Attendance user) {
        attendanceList.set(index, user);
        save();
        return true;
    }
}
