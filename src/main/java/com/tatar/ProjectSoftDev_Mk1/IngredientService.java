/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tatar.ProjectSoftDev_Mk1;

import Dao.IngredientDao;
import java.util.ArrayList;
import model.Ingredient;

/**
 *
 * @author tatar
 */
public class IngredientService {
    private static IngredientDao dao = new IngredientDao();
    
    
    public static ArrayList<Ingredient> getOutSoon(){
        ArrayList<Ingredient> outsoon=dao.getOutofStock();
        return outsoon;
    }
}
